import Vue from 'vue'

import App from './App'
import router from './routes'
import store from './store'
import i18n from './locales/i18n'

/**
 * Register Service Worker.
 */
import './registerServiceWorker'

Vue.config.productionTip = false

new Vue({
  router,
  store,
  i18n,
  render: h => h(App),
  renderError(h, err) {
    return h('pre', { style: { color: 'red' } }, err.stack)
  },
}).$mount('#app')
