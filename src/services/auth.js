import client from '../utils/client'

class ServiceAuth {
  constructor() {
    if (!!ServiceAuth.instance) {
      return ServiceAuth.instance
    }

    ServiceAuth.instance = this

    return this
  }

  accessToken = () => {
    const name = 'asd'
    return client().get('/', name)
  }
}

const Auth = new ServiceAuth()

export default Auth

// export default {
//   accessToken: async () => {
//     try {
//       const data = await client().get()
//
//       console.log(data)
//     } catch (e) {
//       console.log(e)
//     }
//   },
//
//   storeToken: (accessToken) => {
//     localStorage.setItem('access_token', accessToken)
//   },
//
//   removeAccessToken: () => {
//     localStorage.removeItem('access_token')
//   },
//
//   getAccessToken: () => localStorage.getItem('access_token'),
// }
