import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const PageNotFound = () => import('../views/errors/404')
const Login = () => import('../views/auth/Login')
const Home = () => import('../views/home/Home')

export const constantRouterMap = [
  {
    path: '/',
    component: Home,
  },
  {
    path: '/login',
    component: Login,
  },
  {
    path: '*',
    component: PageNotFound,
  },
]

const router = new Router({
  mode: 'history',
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRouterMap,
})

// router.beforeEach((to, from, next) => {
//   // redirect to login page if not logged in and trying to access a restricted page
//   const publicPages = ['/login']
//   const authRequired = !publicPages.includes(to.path)
//   const loggedIn = localStorage.getItem('access_token')
//
//   if (authRequired && !loggedIn) {
//     return next('/login')
//   }
//
//   return next()
// })
//
export default router
