import Vue from 'vue'
import VueI18n from 'vue-i18n'
import Cookies from 'js-cookie'

import enLocale from './lang/en'
import jaLocale from './lang/ja'

Vue.use(VueI18n)

const messages = {
  en: {
    ...enLocale,
  },
  ja: {
    ...jaLocale,
  },
}

const i18n = new VueI18n({
  locale: Cookies.get('language') || 'en',
  fallbackLocale: 'en',
  messages,
})

export default i18n
